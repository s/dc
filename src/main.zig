const std = @import("std");

pub const linux_sysv_shutdown: []const []const u8 = &[_][]const u8 { "init", "0" };
pub const linux_systemd_shutdown: []const []const u8 = &[_][]const u8 { "systemctl", "poweroff" };
pub const windows_shutdown: []const []const u8 = &[_][]const u8 { "shutdown", "/s", "/f" };

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    const shutdown: []const []const u8 = switch(std.builtin.os.tag) {
        .linux => blk: {
            var systemd_dir_or_error = std.fs.Dir.openDir(std.fs.cwd(), "/run/systemd/system", std.fs.Dir.OpenDirOptions {});
            if(systemd_dir_or_error) |*dir| {
                dir.close();
                break :blk linux_systemd_shutdown;
            } else |err| {
                break :blk linux_sysv_shutdown;
            }
        },
        .windows => windows_shutdown,
        else => return error{OsNotSupported}.OsNotSupported,
    };

    const alloc = &gpa.allocator;

    const child = try std.ChildProcess.init(shutdown, alloc);
    try child.spawn();
    _ = try child.wait();
}

